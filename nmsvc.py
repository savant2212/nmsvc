from http.server import BaseHTTPRequestHandler, HTTPServer
import pynmea2
import json
import threading
import time
import serial
import io
import sys
import argparse

#locking not optimal, but it's not high load so leave as is
class GNSSData:
    def __init__(self):
        self._data = {}
        self._lock = threading.Lock()

    def set_data(self, sat, lat, lat_dir, lng, lng_dir, time):
        self._lock.acquire()
        self._data["latitude"] = lat
        self._data["lat_dir"] = lat_dir
        self._data["longitude"] = lng
        self._data["lon_dir"] = lng_dir
        self._data["timestamp"] = time
        self._data["satelites"] = sat
        self._lock.release()

    def get_data(self):
        self._lock.acquire()
        d = self._data.copy()
        self._lock.release()
        
        return d


gnss = GNSSData()
run = 0;

class nmea_thread(threading.Thread):
    def __init__(self, path):
        threading.Thread.__init__(self)
        self._path = path

    def run(self):
        fd = serial.Serial(self._path)
        w = io.TextIOWrapper(fd,  line_buffering=True)
        streamreader = pynmea2.NMEAStreamReader(w)
        data = {'sat' : 0, 'lat' : "0.0", 'lon' : "0.0", 'time':0, 'lon_dir' : "", 'lat_dir':""}

        while(run != 0):
            try:
                for msg in streamreader.next():
                    if msg.sentence_type == "GSA": # satinfo
                        data['sat'] = 12;
                        for i in range(1,13):
                            if msg.data[msg.name_to_idx["sv_id{:02}".format(i)]] == '' :
                                data['sat'] = i
                                break
                    if msg.sentence_type == "RMC": # pos-vel-time
                        data["lat"] = float(msg.lat)
                        data["lon"] = float(msg.lon)
                        data["lat_dir"] = msg.lat_dir
                        data["lon_dir"] = msg.lon_dir
                        data["time"] = int(time.mktime( msg.datetime.timetuple()))
                gnss.set_data(data["sat"], data["lat"], data["lat_dir"], data["lon"], data["lon_dir"], data["time"]);
            except serial.serialutil.SerialException as e:
                # try to reopen
                streamreader = None
                fd.close()
                w = None
                fd = None
                fd = serial.Serial(self._path)
                w = io.TextIOWrapper(fd,  line_buffering=True)
                streamreader = pynmea2.NMEAStreamReader(w)                
            except Exception as e: 
                print(e)

class handler(BaseHTTPRequestHandler):
    
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/json')
        self.end_headers()
        
        d = gnss.get_data()
	# Send message back to client
        message = str(d)
        # Write content as utf-8 data
        self.wfile.write(bytes(message, "utf8"))
        return 

def run():
    parser = argparse.ArgumentParser(description='gps info daemon.')
    parser.add_argument('--ipaddr', metavar='IP', help='ip address to listen', default="0.0.0.0")
    parser.add_argument('--port', metavar='PORT', type=int, help='port', default="8081")
    parser.add_argument('--tty', metavar='TTY', required=True, help='gps tty')

    args = parser.parse_args()

    run = 1
    t1 = nmea_thread(args.tty)
    t1.start()
    server_address = (args.ipaddr, args.port)
    httpd = HTTPServer(server_address, handler)
    print('running server...')
    httpd.serve_forever()
    run = 0
    t1.join()

run()
